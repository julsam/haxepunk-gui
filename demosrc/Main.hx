import com.haxepunk.Engine;
import com.haxepunk.HXP;
import com.haxepunk.utils.Key;
import nme.Lib;
import world.DemoScene;

class Main extends Engine
{

	override public function init()
	{
#if debug
	#if flash
		if (flash.system.Capabilities.isDebugger)
	#end
		{
			HXP.console.enable();
			HXP.console.toggleKey = Key.P;
		}
#end
		HXP.screen.scale = 2;
		HXP.scene = new DemoScene();
	}

	public static function main()
	{
		new Main();
	}

}